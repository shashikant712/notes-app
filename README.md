# Notes-App

Notes App is developed in JavaScript and NodeJS and works in terminal. User provides specified input in terminal and get outputs in terminal too. Notes App is used to Create
a Note, Read a Note, Remove a Note and Read all Notes. In this App [Chalk](https://www.npmjs.com/package/chalk) and [Yargs](https://www.npmjs.com/package/yargs) npm packages has been used.

#### Demo : [CodeSandBox](https://codesandbox.io/s/notes-app-90fu9)

###### ( Add a new writable terminal in codesandbox and run the commnads )

## Commands :

###### ( Run commands in terminal )

### Create a New Note

`node index.js add --title="Buy Soap" --body="Dettol Bathing Soap MRP-27"`

![Add Note](/Images/addNote.png)

If note already present, It will show error

![Note Exists](/Images/alreadyAdded.png)

### Remove a Note

`node index.js remove --title="Buy Soap"`

![Remove Note](/Images/removeNote.png)

If note is not present in JSON file, then it will show error

![Not Found](/Images/notFound.png)

### Read a Note

`node index.js read --title="Buy Soap"`

![Read Note](/Images/readNote.png)

If note not found then it will show an error

![Not Found](/Images/notFound.png)

### List All Notes

`node index.js list`

![List Note](/Images/listNote.png)

If No notes in JSON File, then it will show something like this

![No Notes](/Images/emptyData.png)
### Yargs Version

`node index.js --version`

![App Version](/Images/appVersion.png)

### Help Command

`node index.js --help`

![Help Command](/Images/helpCommand.png)

If you need help in specific command. eg: need help in `add` command -

`node index.js add --help`

![Help In Add Command](/Images/helpInAdd.png)

## Built With :

- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [NodeJS](https://nodejs.org/en/)
- [Chalk](https://www.npmjs.com/package/chalk)
- [Yargs](https://www.npmjs.com/package/yargs)

## Developed By :

- [Shashi Kant Yadav](https://bitbucket.org/%7B5374df3f-07c6-4a04-aa51-3e2f5760fa6a%7D/)

## Guided By ( through course ) :

- [Andrew Mead](https://github.com/andrewjmead)
